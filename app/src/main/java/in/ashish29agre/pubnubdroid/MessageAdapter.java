package in.ashish29agre.pubnubdroid;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class MessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private final List<String> messages;

        MessageAdapter(List<String> messages) {
            this.messages = messages;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new TextViewHolder(new TextView(parent.getContext()));
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            TextView textView = (TextView) holder.itemView;
            textView.setText(messages.get(position));
        }

        @Override
        public int getItemCount() {
            return messages.size();
        }

        class TextViewHolder extends RecyclerView.ViewHolder {

            public TextViewHolder(View itemView) {
                super(itemView);
            }
        }
    }