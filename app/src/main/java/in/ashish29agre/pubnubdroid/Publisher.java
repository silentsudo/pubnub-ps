package in.ashish29agre.pubnubdroid;

import com.pubnub.api.Callback;
import com.pubnub.api.Pubnub;
import com.pubnub.api.PubnubError;

import org.json.JSONObject;

/**
 * Created by Ashish on 09/01/16.
 */
public class Publisher extends Callback {
    private Pubnub pubnub;
    private PublisherCallback callback;
    private String channel;

    Publisher(PubnubManager pubnubManager, PublisherCallback callback, String channel) {
        pubnub = pubnubManager.getPubnub();
        this.channel = channel;
        this.callback = callback;
    }

    public void publish(JSONObject object) {
        pubnub.publish(channel, object, this);
    }

    public void publish(String object) {
        pubnub.publish(channel, object, this);
    }


    @Override
    public void successCallback(String channel, Object message) {
        super.successCallback(channel, message);
        callback.onSuccess(message);
    }


    @Override
    public void errorCallback(String channel, PubnubError error) {
        super.errorCallback(channel, error);
        callback.onFailure(error);
    }

    interface PublisherCallback {

        void onSuccess(Object message);

        void onFailure(PubnubError error);
    }
}
